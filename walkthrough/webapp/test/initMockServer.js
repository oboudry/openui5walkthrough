sap.ui.define([
    "../localService/mockserver"
], function(mockserver) {
    "use strict";

    // initialize the mock server
    mockserver.init();

    // initialize de embedded component on the HTML page
    sap.ui.require(["sap/ui/core/ComponentSupport"]);
})